
// Set the date we're counting down to
var countDownDate = new Date("August 13, 2021 16:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  // convert number to a string, then extract the first digit
  var one = String(days).charAt(0);
  var two = String(days).charAt(1);
  // convert the first digit back to an integer
  var days_1 = Number(one);
  var days_2 = Number(two);

  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
  document.getElementById("dDays").innerHTML = `
    <div class="row">
        <div class="col-3">
            <p class="counter-time">`+ days + `</p>
            <p>DAYS</p>
        </div>
        <div class="col-3">
            <p class="counter-time">`+ hours + `</p>
            <p>HOUR</p>
        </div>
        <div class="col-3">
            <p class="counter-time">`+ minutes + `</p>
            <p>MINUTE</p>
        </div>
        <div class="col-3">
            <p class="counter-time-last">`+ seconds + `</p>
            <p>SECOND</p>
        </div>
    </div>`;

  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    // document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);

