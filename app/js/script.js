const openInvite = document.querySelector('.openInvite');
const landingPage = document.querySelector('.landing-page');
const bufferPage = document.querySelector('.buffer-page');
const frontPage = document.querySelector('.front-page');
const playmusic = document.querySelector('#play');
const stopmusic = document.querySelector('#stop');
const music = document.getElementById("musicWish");
const btnProkes = document.querySelector('.prokes-btn');
const prokesDis = document.querySelector("#covid-dis");
openInvite.addEventListener("click", slideUp);


function slideUp() {
    landingPage.style.animation = "slideUp 1.0s linear";
    // frontPage.style.animation = "slideUp 1.5s linear";
    
    window.scrollTo(0, 0);
    setTimeout(() => {
        landingPage.remove();
        music.play();
        // bufferPage.remove();
    }, 1000);
}

playmusic.addEventListener("click", function(){
    music.play();
    console.log('on');
    stopmusic.classList.toggle('d-none');
    playmusic.classList.toggle('d-none');
})

stopmusic.addEventListener("click", function(){
    music.pause();
    console.log('off');
    playmusic.classList.toggle('d-none');
    stopmusic.classList.toggle('d-none');

})

btnProkes.addEventListener("click", function(){
    prokesDis.remove();
})







